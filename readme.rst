Uwalls
======
Useful wallpapers, shortly Uwalls, let's set actually a useful wallpaper
(at least for a few days) on your desktop!

Why?
----
Have you ever learned something and couldn't remember sh-t? You probably
used stickers and put them all around your screen, table or fridge. Well
why not put them right "into" the screen? So here we go.

You look to your screen many hours a day and I guess this might help you
to actually learn/remember stuff.

No watermark
------------
I've decided not to put any sign or watermark into the wallpapers.
There is no good reason to advertise this repository at the expense of the
wallpapers quality. Feel free to link this repository to anywhere you
post or share your wallpaper so other people can come here for more.

Contribute
----------
I would be very happy if you open a pull request with your own wallpaper.
The more people contribute the better this project will become.

Formats
~~~~~~~
Every wallpaper must have it's source (Gimp .xcf file) and a few exports.
As late 2018 folks uses these resolutions the most:

* 1080p: 1920 x 1080
* 1440p: 2560 x 1440
* 4K UHD: 4096 x 2160

So your request should include there resolutions. Furtunately wallpaper
background is a flat color so there is no problem to scale it at all.

Sizing
~~~~~~
Whole info part has to be horizontaly and verticaly centered. Also
shouldn't be wider/taller than 33% of the whole wallpaper

.. image:: wall_layout.png

Section has to have 50px span from top/bottom, 200px span from left/right.
That gives us nice airy grid.

Colors
~~~~~~
Background: #1a1a1a
Foreground: #e6e6e6

Font
~~~~
Tamzen 13px (included in the repository).
